<?php
/**
 * Created by PhpStorm.
 * User: FAJAR FITRI ROZI
 * Date: 28/06/2019
 * Time: 17:31
 */

namespace App\Http\Middleware;

use Closure;
use App\Model\User;

class AuthBasic
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        if (!$token) {
            return response()->json(['message' => 'Token not provided'], 401);
        }

        $user = User::where('token', $token)->first();
        if ($user) {
            $request->merge(['user' => $user]);
            return $next($request);
        } else {
            return response()->json([
                'message' => 'Provided token is expired. You need to login again'
            ], 401);
        }
    }
}