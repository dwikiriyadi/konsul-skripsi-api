<?php
/**
 * Created by PhpStorm.
 * User: FAJAR FITRI ROZI
 * Date: 28/06/2019
 * Time: 17:20
 */

namespace App\Http\Controllers;


use App\Model\Guidance;
use App\Model\User;
use Illuminate\Http\Request;

class GuidanceController extends Controller
{
    public function index(Request $request)
    {
        $token = $request->header("Authorization");

        $user = User::where('token', $token)->first();

        if ($user->role == "lecturer") {
            if($request->has('id')) {
                $guidance = Guidance::where('id', $request->id)->first();

                return response()->json(['result' => $guidance], 200);
            }

            $guidances = Guidance::where('student_id', $request->student_id)->get();

            return response()->json(['result' => $guidances], 200);
        } else {
            if($request->has('id')) {
                $guidance = Guidance::where('id', $request->id)->first();

                return response()->json(['result' => $guidance], 200);
            }

            $guidances = Guidance::where('student_id', $user->userable_id)->get();

            return response()->json(['result' => $guidances], 200);
        }
    }

    public function store(Request $request)
    {
        $token = $request->header("Authorization");

        $user = User::where('token', $token)->first();

        Guidance::create([
            'student_id' => $user->userable_id,
            'title' => $request->title,
            'body' => $request->body
        ]);

        return response()->json(['messages' => 'Data has been added'], 200);
    }

    public function update($id, Request $request)
    {
        $token = $request->header("Authorization");

        $user = User::where('token', $token)->first();

        $guidance = Guidance::where('id', $id);

        if ($user->role == 'student') {
            $guidance->update([
                'title' => $request->title,
                'body' => $request->body
            ]);

            return response()->json(['messages' => 'Data has been updated'], 200);
        } else {
            $guidance->update([
                'status' => $request->status,
            ]);

            return response()->json(['messages' => 'You are approved this guidance'], 200);
        }
    }

    public function destroy($id)
    {
        Guidance::where('id', $id)->delete();

        return response()->json(['messages' => 'Data has been deleted'], 200);
    }
}
