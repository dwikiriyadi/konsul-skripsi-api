<?php
/**
 * Created by PhpStorm.
 * User: FAJAR FITRI ROZI
 * Date: 28/06/2019
 * Time: 17:20
 */

namespace App\Http\Controllers;

use App\Model\User;
use App\Model\Lecturer;
use App\Model\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UserController extends Controller
{
    public function index(Request $request)
    {
        $token = $request->header("Authorization");

        $user = User::where('token', $token)->first();

        $data = $user->userable;
        $data->role = $user->role;

        return response()->json(['result' => $data], 200);
    }

    public function student(Request $request) {
        $token = $request->header("Authorization");

        $user = User::where('token', $token)->first();

        $data = Student::where('lecturer_id', $user->userable_id)->get();

        return response()->json(['result' => $data], 200);
    }

    public function store(Request $request)
    {
        if ($request->role == 'student') {

            $student = Student::create([
                'name' => $request->name,
                'id_number' => $request->id_number,
                'image_url' => $request->image_url,
                'lecturer_id' => $request->lecturer_id
            ]);

            $user = new User([
                'username' => $request->id_number,
                'password' => Hash::make($request->password),
                'role' => 'student'
            ]);

            $student->user()->save($user);

            return response()->json([
                'message' => 'Student has been added'
            ]);
        } else {
            $lecturer = Lecturer::create([
                'name' => $request->name,
                'id_number' => $request->id_number,
                'image_url' => $request->image_url,
            ]);

            $user = new User([
                'username' => $request->id_number,
                'password' => Hash::make($request->password),
                'role' => 'lecturer'
            ]);

            $lecturer->user()->save($user);

            return response()->json([
                'message' => 'Lecturer has been added'
            ]);
        }


    }

    public function update(Request $request)
    {

    }

    public function destroy(Request $request)
    {

    }

    public function login(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 412);
        }

        $user = User::where('username', $request->username)->first();

        if (!$user) {
            return response()->json([
                'message' => 'Username not found'
            ], 404);
        }

        if (Hash::check($request->password, $user->password)) {

            if ($user->token === null) {
                $token = Str::random(50);

                User::where('username', $request->username)->update(['token' => $token]);

                return response()->json([
                    'messages' => 'Sign in success',
                    'result' => $token
                ], 200);
            }

            return response()->json([
                'message' => 'You are already login on other device'
            ], 401);
        }

        return response()->json([
            'message' => 'Username or password is wrong'
        ], 404);
    }

    public function logout(Request $request)
    {
        $token = $request->header("Authorization");

        User::where('token', $token)->update(['token' => null]);

        return response()->json(['messages' => "You are logged out"], 200);
    }
}
