<?php
/**
 * Created by PhpStorm.
 * User: FAJAR FITRI ROZI
 * Date: 28/06/2019
 * Time: 17:09
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    protected $fillable = ['name', 'id_number', 'image_url'];

    public function user()
    {
        return $this->morphOne('App\Model\User', 'userable');
    }

    public function students()
    {
        return $this->hasMany('App\Model\Student');
    }
}