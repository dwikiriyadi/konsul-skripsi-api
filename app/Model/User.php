<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['username','password', 'role', 'token', 'userable_id', 'userable_type'];

    public function userable()
    {
        return $this->morphTo();
    }

}
