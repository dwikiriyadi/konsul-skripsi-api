<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['name', 'id_number', 'image_url', 'lecturer_id'];

    public function user()
    {
        return $this->morphOne('App\Model\User', 'userable');
    }

    public function lecturer()
    {
        return $this->belongsTo('App\Model\Lecturer');
    }

    public function guidances()
    {
        return $this->hasMany('App\Model\Guidance');
    }
}