<?php
/**
 * Created by PhpStorm.
 * User: FAJAR FITRI ROZI
 * Date: 28/06/2019
 * Time: 17:14
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Guidance extends Model
{
    protected $fillable = ['student_id', 'title', 'body', 'status'];

    public function student()
    {
        return $this->belongsTo('App\Model\Student');
    }
}
