<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Str;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('signin', 'UserController@login');
$router->post('user', 'UserController@store');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('signout', 'UserController@logout');
    $router->get('user', 'UserController@index');
    $router->get('student', 'UserController@student');

    $router->get('guidance', 'GuidanceController@index');
    $router->post('guidance', 'GuidanceController@store');
    $router->post('guidance/{id}', 'GuidanceController@update');
    $router->delete('guidance/{id}', 'GuidanceController@destroy');
});
